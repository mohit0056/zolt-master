package com.zolt.trustexchange;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements  NetworkCall.MyNetworkCallBack,NewViewpagerAdapter.pageonClick,BottomNavigationView.OnNavigationItemSelectedListener{
    ImageView profileBTN;
    RelativeLayout kyc_verify;
    ViewPager viewPager;
    TabLayout tabLayout;
    BottomNavigationView bottomBar;
    public static ArrayList<Bannerpojo> listBanners = new ArrayList<>();
    Bannerpojo bannerpojo;
    Progress progress;
    NetworkCall networkCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = new Progress(MainActivity.this);
        networkCall=new NetworkCall(MainActivity.this,MainActivity.this);
        bannerload();


        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);


        tabLayout.addTab(tabLayout.newTab().setText("Buy Funjo Coins"));
//        tabLayout.addTab(tabLayout.newTab().setText("Losers"));
//        tabLayout.addTab(tabLayout.newTab().setText("24h Vol"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final MyAdapter adapter = new MyAdapter(this,getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        bottomBar = (BottomNavigationView) findViewById(R.id.bottomBar);
        bottomBar.setOnNavigationItemSelectedListener(MainActivity.this);
        bottomBar.setSelectedItemId(R.id.item0);
       profileBTN = findViewById(R.id.profileIconMain);

       kyc_verify = (RelativeLayout)findViewById(R.id.KYcContent);
       kyc_verify.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent ky = new Intent(MainActivity.this,Verify_KYC.class);
               startActivity(ky);
               finish();
           }
       });


    }

    private void bannerload() {
        networkCall.NetworkAPICall("http://inmortaltech.com/Balvahan-APIs/public/api/banner_bit",true);

    }


    public void goback(View view) {

        Intent intent4 = new Intent(MainActivity.this,Profile.class);
        startActivity(intent4);
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.item1:
                Intent intentu = new Intent(MainActivity.this, markets.class);
                startActivity(intentu);
                break;
            case R.id.item2:
                Intent intentc = new Intent(MainActivity.this, trades_main.class);
                startActivity(intentc);
                break;
            case R.id.item3:
                Intent intent_market = new Intent(MainActivity.this, futures.class);
                startActivity(intent_market);
                break;
            case R.id.item4:
                Intent WALLET = new Intent(MainActivity.this, Wallet.class);
                startActivity(WALLET);
                break;
        }

        return true;
    }

    @Override
    public void viewPageImageClick(String page, int position) {

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case "http://inmortaltech.com/Balvahan-APIs/public/api/banner_bit":
                ion = (Builders.Any.B) Ion.with(MainActivity.this)
                        .load("GET", "http://inmortaltech.com/Balvahan-APIs/public/api/banner_bit");
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case "http://inmortaltech.com/Balvahan-APIs/public/api/banner_bit":
                try {
                    JSONObject jsonObject=new JSONObject(jsonstring.toString());
                    String success = jsonObject.getString("success");
                    if (success.equals("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            bannerpojo = new Gson().fromJson(jsonArray.optJSONObject(i).toString(), Bannerpojo.class);
                            listBanners.add(bannerpojo);
                        }

                        ViewPager viewPagerBanners = (ViewPager) findViewById(R.id.main_grid_vf_banner);
                        viewPagerBanners.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                for (int i = 0; i < listBanners.size(); i++) {

                                }

                            }

                            @Override
                            public void onPageSelected(int position) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });

                        NewViewpagerAdapter viewPagerAdapter = new NewViewpagerAdapter(this, listBanners, this, "offer");
                        viewPagerBanners.setAdapter(viewPagerAdapter);
                        viewPagerAdapter.notifyDataSetChanged();

                    }
                    else{

                        progress.dismiss();
                    }
                }
                catch (JSONException e1) {
                    Toast.makeText(MainActivity.this,""+e1, Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(MainActivity.this,jsonstring, Toast.LENGTH_SHORT).show();

    }

}