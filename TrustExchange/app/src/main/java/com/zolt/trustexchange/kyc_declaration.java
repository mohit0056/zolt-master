package com.zolt.trustexchange;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link kyc_declaration#newInstance} factory method to
 * create an instance of this fragment.
 */
public class kyc_declaration extends Fragment implements NetworkCall.MyNetworkCallBack{
    ImageView imageViewfront, imageViewback;
    private static final int SELECT_PICTURE = 1;
    SharedPreferences mSharedPreference;
    int clickImage;
    private static final int PICK_FROM_GALLERY = 1;
    String profession_string="";
    String filePath2="";
    Uri contentURI;
    File File_data;
    File File_data2;
    ImageView pancard_img;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private static final int IMAGE_PICKER_SELECT1 = 1;
    private static final int IMAGE_PICKER_SELECT2 = 1;
    EditText ent_amount;
    private String selectedImagePath,userid;
    Progress progress;
    NetworkCall networkCall;
    Uri selectedMediaUri,selctvideouri,uri;
    Button upload_data;
    Boolean cancel;

    SharedPreferences.Editor editor;
EditText profession_txt;

    String adharfrontfilePath="",adharbackfilepath="",pancardfilePath="";

    final int RESULT_LOAD_IMG = 1000;

    private ImageView img;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public kyc_declaration() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment kyc_declaration.
     */
    // TODO: Rename and change types and number of parameters
    public static kyc_declaration newInstance(String param1, String param2) {
        kyc_declaration fragment = new kyc_declaration();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_kyc_declaration, container, false);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        editor = prefs.edit();

        progress = new Progress(getContext());
        networkCall = new NetworkCall(kyc_declaration.this, getContext());

        userid=(mSharedPreference.getString("id", ""));
profession_txt=(EditText)v.findViewById(R.id.profession);



        adharfrontfilePath=(mSharedPreference.getString("aadharfrontfilePath", ""));
        adharbackfilepath=(mSharedPreference.getString("aadharbackfilePath", ""));
        pancardfilePath=(mSharedPreference.getString("pancardfilePath", ""));


        upload_data = (Button) v.findViewById(R.id.upload_data);
        upload_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profession_string = profession_txt.getText().toString().trim();

                profession_txt.setError(null);
                profession_txt.setError(null);

                if (TextUtils.isEmpty(profession_string)) {
                    profession_txt.setError("First Name is required");
                    cancel = true;
                }  else {

                    UploadData();


                }
            }
        });

        return v;
    }

    private void UploadData() {

        networkCall.NetworkAPICall(ApiURL.kyc_verification, true);


    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.kyc_verification:
                ion = (Builders.Any.B) Ion.with(getContext())
                        .load("POST", ApiURL.kyc_verification)
                        .setMultipartFile("adharcard_front", new File(adharfrontfilePath))
                        .setMultipartFile("adharcard_back", new File(adharbackfilepath))
                        .setMultipartFile("pancard", new File(pancardfilePath))
                        .setMultipartParameter("decalration", profession_string)
                        .setMultipartParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.kyc_verification:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        Toast.makeText(getContext(), "You have successfully uploaded the documents", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(getContext(), "Please submit data in every field".toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(getContext(), jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(getContext(), jsonstring, Toast.LENGTH_SHORT).show();

    }
}