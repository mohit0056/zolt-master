package com.zolt.trustexchange.utill;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

public class NetworkCall {
    MyNetworkCallBack myCBI;
    public ProgressDialog mprogress;


    Progress progress;
    Context context;
    String id;
    private static final String TAG = "NetworkCall";

    public NetworkCall(MyNetworkCallBack callBackInterface, Context context) {
//        mprogress = new ProgressDialog(context);
        progress=new Progress(context);
        myCBI = callBackInterface;
        this.context = context;
    }

    public void NetworkAPICall(final String apiType, final boolean showprogress) {
        Log.e("NetworkAPI Interface", "================" + apiType);
        if (Helper.isConnectingToInternet(context)) {
            if (showprogress)
                progress.show();

            myCBI.getAPIB(apiType)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String jsonString) {
                            if (showprogress)
                                progress.dismiss();
                            try {
                                if (e == null) {
                                    if (jsonString != null && !jsonString.isEmpty()) {
                                        Log.d(TAG, "onCompleted: " + jsonString);
                                        JSONObject jsonObject = new JSONObject(jsonString);
                                        myCBI.SuccessCallBack(jsonObject, apiType);
                                    } else {
                                        myCBI.ErrorCallBack(""+e, apiType);
                                    }
                                } else {
//                                        myCBI.ErrorCallBack("Connection error! Please try again", apiType);
                                    myCBI.ErrorCallBack(""+e, apiType);
                                }
                            } catch (JSONException e1) {
                                myCBI.ErrorCallBack(""+e, apiType);
//                                    myCBI.ErrorCallBack("Connection error! Please try again", apiType);
                                Log.d(TAG, "onCompleted Catch: " + e1);
                                e1.printStackTrace();
                            }
                        }
                    });
        } else {
            myCBI.ErrorCallBack("Connection error! Please try again", apiType);
        }

    }


    public interface MyNetworkCallBack {

        Builders.Any.B getAPIB(String apitype);

        void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException;

        void ErrorCallBack(String jsonstring, String apitype);
    }
}
