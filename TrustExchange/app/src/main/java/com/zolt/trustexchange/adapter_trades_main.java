package com.zolt.trustexchange;


import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class adapter_trades_main extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public adapter_trades_main(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
//            case 0:
//                trades_convert trades_convert = new trades_convert();
//                return trades_convert;
            case 0:
                trades_spot trades_spot = new trades_spot();
                return trades_spot;
//            case 2:
//                trades_margin trades_margin = new trades_margin();
//                return trades_margin;
//            case 3:
//                trades_fiat trades_fiat = new trades_fiat();
//                return trades_fiat;
//            case 4:
//                trades_p2p trades_p2p = new trades_p2p();
//                return trades_p2p;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}
