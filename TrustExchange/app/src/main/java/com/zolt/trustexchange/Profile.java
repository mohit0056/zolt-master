package com.zolt.trustexchange;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class Profile extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    TextView name, email, phone;
    RelativeLayout profileLogout, verifykyc, inr_wallets;
    LinearLayout changepasswordbtn;
    Button profilebtn;
    ImageView profile_btn_edit;
    private static final int IMAGE_PICKER_SELECT = 1;
    Uri selectedMediaUri;
    String filePath = "", customerid = "";
    File File_data;
    SharedPreferences mSharedPreference;
    EditText customernam, customerlast, profileemail, profilemobileno, profileaddress;
    String mcustomernam = "", mcustomerlast = "", mprofileemail = "", mprofilemobileno = "", mprofileaddress = "";
    Progress progress;
    NetworkCall networkCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        progress = new Progress(Profile.this);
        networkCall = new NetworkCall(Profile.this, Profile.this);

        name = (TextView) findViewById(R.id.profile_Name);
        email = (TextView) findViewById(R.id.profile_Gmail);
        profileLogout = (RelativeLayout) findViewById(R.id.profile_logout);
        profileLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logout();
//                Intent intent = new Intent(Profile.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(Profile.this).edit();
//                editor.remove("login");//your key
//                editor.clear();
//                editor.commit();
            }
        });

        profil();

        verifykyc = (RelativeLayout) findViewById(R.id.verify_kyc);
        verifykyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kyc = new Intent(Profile.this, Verify_KYC.class);
                startActivity(kyc);

            }
        });



    }

    private void logout() {
        networkCall.NetworkAPICall(ApiURL.user_logout, true);

    }

    private void profil() {
        networkCall.NetworkAPICall(ApiURL.user_profile, true);
    }

    public void goback(View view) {
        super.onBackPressed();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_profile:
                ion = (Builders.Any.B) Ion.with(Profile.this)
                        .load("GET", ApiURL.user_profile);

                break;

            case ApiURL.user_logout:
                ion = (Builders.Any.B) Ion.with(Profile.this)
                        .load("GET", ApiURL.user_logout);

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.user_profile:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("isAuth");
                    if (status.equals("true")) {

                        String id = jsonObject.getString("id");
                        String memail = jsonObject.getString("email");
                        String mname = jsonObject.getString("name");

                        name.setText(mname);
                        email.setText(memail);

                    } else {


                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(Profile.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.user_logout:
                try {
                    JSONObject jsonObject1 = new JSONObject(jsonstring.toString());
                    String status = jsonObject1.getString("error");
                    if (status.equals("true")) {
                        Intent intent = new Intent(Profile.this, main_activity_after_registration.class);
                        startActivity(intent);
                        finish();
                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(Profile.this).edit();
                        editor.remove("login");//your key
                        editor.clear();
                        editor.commit();
                    } else {


                    }
                } catch (JSONException e1) {
                    Toast.makeText(Profile.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;


        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Profile.this, "Are You Sure ?", Toast.LENGTH_SHORT).show();

    }
}