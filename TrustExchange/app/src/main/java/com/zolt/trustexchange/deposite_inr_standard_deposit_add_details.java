package com.zolt.trustexchange;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;
import com.zolt.trustexchange.utill.PathUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class deposite_inr_standard_deposit_add_details extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    ImageView imageView;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private static final int IMAGE_PICKER_SELECT = 1;
    EditText ent_amount;
    private String selectedImagePath;
    int clickImage;
    Uri selectedMediaUri,selctvideouri,uri;
    Button upload;
    Uri contentURI;
    File File_data;
    Progress progress;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    String myEnteramount, myUploadimage,path="", userid,filename = "",filePath="",filenameimg="";
    final int RESULT_LOAD_IMG = 1000;
    private static final int PICK_FROM_GALLERY = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposite_inr_standard_deposit_add_details);

        progress = new Progress(deposite_inr_standard_deposit_add_details.this);
        networkCall = new NetworkCall(deposite_inr_standard_deposit_add_details.this, deposite_inr_standard_deposit_add_details.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid=(mSharedPreference.getString("id", ""));


        imageView = (ImageView) findViewById(R.id.upload_img);
        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(deposite_inr_standard_deposit_add_details.this,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(deposite_inr_standard_deposit_add_details.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=1;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ent_amount = (EditText) findViewById(R.id.EnterAmount);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        upload = (Button) findViewById(R.id.upload_data);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myEnteramount = ent_amount.getText().toString().trim();


                ent_amount.setError(null);


                uploadData();
            }
        });
    }

    public void goback(View view) {
        super.onBackPressed();
    }

    private void loadImagefromGallery() {

        if (clickImage == 1) {

            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
            if (data != null) {
                contentURI = data.getData();
                try {
                    Uri selectedImage = data.getData();
                    imageView.setImageURI(selectedImage);
                    filePath = getRealPathFromURI(selectedImage);
                    File_data = new File(filePath);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }



        super.onActivityResult(requestCode, resultCode, data);
    }
    private String getRealPathFromURI(Uri uri) {

        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void uploadData() {
        networkCall.NetworkAPICall(ApiURL.Deposite_money, true);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.Deposite_money:
                ion = (Builders.Any.B) Ion.with(deposite_inr_standard_deposit_add_details.this)
                        .load("POST", ApiURL.Deposite_money)
                        .setMultipartParameter("user_id", userid)
                        .setMultipartParameter("wallet_amount", myEnteramount)
                        .setMultipartFile(" bitimage", new File(filePath));


                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.Deposite_money:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        Toast.makeText(deposite_inr_standard_deposit_add_details.this, "successfully Uploaded", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(deposite_inr_standard_deposit_add_details.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(deposite_inr_standard_deposit_add_details.this, "Your Account is on hold...".toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(deposite_inr_standard_deposit_add_details.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(deposite_inr_standard_deposit_add_details.this,""+jsonstring, Toast.LENGTH_SHORT).show();

    }
}