package com.zolt.trustexchange;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link spot#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class spot extends Fragment {
    ViewPager mViewPager,viewPager;
    TabLayout tabLayout;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment spot.
     */
    // TODO: Rename and change types and number of parameters
    public static spot newInstance(String param1, String param2) {
        spot fragment = new spot();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public spot() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_spot, container, false);
        tabLayout = v.findViewById(R.id.spotItemsLayout);
        viewPager = v.findViewById(R.id.spotItemsPager);
        tabLayout.addTab(tabLayout.newTab().setText("FAV"));
        tabLayout.addTab(tabLayout.newTab().setText("BTC"));
        tabLayout.addTab(tabLayout.newTab().setText("USDT"));
        tabLayout.addTab(tabLayout.newTab().setText("INR"));
        tabLayout.addTab(tabLayout.newTab().setText("TRX"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        adapter_spot adapter = new adapter_spot(getActivity(),getFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    return v;

    }
}