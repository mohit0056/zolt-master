package com.zolt.trustexchange;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public class trades_main extends AppCompatActivity {
    ViewPager mViewPager,viewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trades_main);

        tabLayout = findViewById(R.id.trades_main_tab);
        viewPager = findViewById(R.id.trades_main_Pager);
      //  tabLayout.addTab(tabLayout.newTab().setText("Convert"));
        tabLayout.addTab(tabLayout.newTab().setText("Spot"));
//        tabLayout.addTab(tabLayout.newTab().setText("Margin"));
//        tabLayout.addTab(tabLayout.newTab().setText("Fiat"));
//        tabLayout.addTab(tabLayout.newTab().setText("P2P"));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final adapter_trades_main adapter = new adapter_trades_main(this,getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    public void goback(View view) {
        super.onBackPressed();
    }
}