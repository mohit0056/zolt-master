package com.zolt.trustexchange.utill;

import android.content.Context;
import android.content.SharedPreferences;


import com.google.gson.Gson;
import com.google.gson.internal.Primitives;
import com.google.gson.reflect.TypeToken;
import com.zolt.trustexchange.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class PrefrenceHelper {
    private static PrefrenceHelper instance;
    private SharedPreferences prefs;

    public static PrefrenceHelper getInstance() {
        return instance;
    }

    private PrefrenceHelper(Context context) {
        prefs = context.getApplicationContext().getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    private PrefrenceHelper(Context context, String sharePreferencesName) {
        prefs = context.getApplicationContext().getSharedPreferences(sharePreferencesName, Context.MODE_PRIVATE);
    }

    public static PrefrenceHelper initHelper(Context context) {
        if (instance == null)
            instance = new PrefrenceHelper(context);
        return instance;
    }

    public static PrefrenceHelper initHelper(Context context, String sharePreferencesName) {
        if (instance == null)
            instance = new PrefrenceHelper(context, sharePreferencesName);
        return instance;
    }

    public void setValue(String KEY, boolean value) {
        prefs.edit().putBoolean(KEY, value).apply();
    }

    public void setValue(String KEY, String value) {
        prefs.edit().putString(KEY, value).apply();
    }

    public void setValue(String KEY, Object value) {
        prefs.edit().putString(KEY, new Gson().toJson(value)).apply();
    }

    public void setValue(String KEY, int value) {
        prefs.edit().putInt(KEY, value).apply();
    }

    public void setValue(String KEY, long value) {
        prefs.edit().putLong(KEY, value).apply();
    }

    public void setValue(String KEY, float value) {
        prefs.edit().putFloat(KEY, value).apply();
    }

    public void setValue(String KEY, double defValue) {
        setValue(KEY, String.valueOf(defValue));
    }

    public <T> void setValue(String KEY, List<T> strings) {
        setValue(KEY, new Gson().toJson(strings));
    }

    public <T> void setValue(String KEY, T[] array) {
        JSONArray jArray = new JSONArray();
        for (T t : array) {
            jArray.put(t);
        }
        prefs.edit().putString(KEY, new Gson().toJson(jArray)).apply();
    }

    public <T> T[] getArrayValue(String KEY) {
        T[] results = null;
        try {
            JSONArray jArray = new JSONArray(prefs.getString(KEY, ""));
            for (int i = 0; i < jArray.length(); i++) {
                results[i] = (T) jArray.get(i);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return results;
    }

    public <T> ArrayList<T> getListValue(String KEY) {
        List<T> objects = null;
        try {
            String obj = prefs.getString(KEY, "");
            objects = new Gson().fromJson(obj, new TypeToken<List<T>>() {
            }.getType());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return (ArrayList<T>) objects;
    }

    public boolean getBooleanValue(String KEY, boolean defvalue) {
        return prefs.getBoolean(KEY, defvalue);
    }

    public boolean getBoolean(String key) {
        return prefs.getBoolean(key, false);
    }

    public String getStringValue(String KEY, String defvalue) {
        return prefs.getString(KEY, defvalue);
    }

    public boolean getKeyContain(String KEY) {
        return prefs.contains(KEY);
    }

    public <T> T getObjectValue(String KEY, Class<T> mModelClass) {
        Object object = null;
        try {
            object = new Gson().fromJson(prefs.getString(KEY, ""), mModelClass);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Primitives.wrap(mModelClass).cast(object);
    }

    public int getIntValue(String KEY, int defValue) {
        return prefs.getInt(KEY, defValue);
    }

    public long getLongValue(String KEY, long defValue) {
        return prefs.getLong(KEY, defValue);
    }

    public float getFloatValue(String KEY, float defValue) {
        return prefs.getFloat(KEY, defValue);
    }

    public double getDoubleValue(String KEY, double defValue) {
        return Double.parseDouble(getStringValue(KEY, String.valueOf(defValue)));
    }

    public void removeKey(String KEY) {
        prefs.edit().remove(KEY).commit();
    }

    public void clearAll() {
        prefs.edit().clear();
        prefs.edit().commit();
    }

    public boolean contain(String KEY) {
        return prefs.contains(KEY);
    }

    public void registerChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        prefs.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        prefs.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public void setObject(String key, Object object) {
        prefs.edit().putString(key, new Gson().toJson(object)).commit();
//prefs.edit().apply();
    }

    public String getBhajanString(String key) {
        Object result = null;
        String strJson = prefs.getString(key, null);
        return strJson;
    }
}
