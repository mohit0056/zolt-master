package com.zolt.trustexchange;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONException;
import org.json.JSONObject;

public class phoneRegistration extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
Button mobileRegister;
    EditText mobile_number,pass_word;
    CheckBox chkCondition;
    Boolean cancel;
    Progress progress;
    NetworkCall networkCall;
    String mymobile_number, mypassword;
    ImageView hidebtn, confhidebtn;
    private boolean isPasswordView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_registration);

        progress = new Progress(phoneRegistration.this);
        networkCall = new NetworkCall(phoneRegistration.this, phoneRegistration.this);

        TextView textView1 = (TextView)findViewById(R.id.registerwithemail);
        mobile_number = (EditText) findViewById(R.id.phoneEditText);
        pass_word = (EditText) findViewById(R.id.passwordmEditText);
        chkCondition = (CheckBox) findViewById(R.id.checkmBox);
        hidebtn = (ImageView)findViewById(R.id.hidemBtn);

        hidebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordView) {
                    hidebtn.setBackgroundResource(R.drawable.view);
                    pass_word.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isPasswordView = false;
                } else {
                    hidebtn.setBackgroundResource(R.drawable.hide);
                    pass_word.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isPasswordView = true;
                }

            }
        });


        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(phoneRegistration.this, emailRegistration.class);
                startActivity(intent);

            }
        });

        mobileRegister = (Button) findViewById(R.id.btnMobileRegister);
        mobileRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mymobile_number = mobile_number.getText().toString().trim();

                mypassword = pass_word.getText().toString().trim();

                mobile_number.setError(null);
                pass_word.setError(null);



                if (TextUtils.isEmpty(mymobile_number)) {
                    mobile_number.setError("Phone number is Required");
                    cancel = true;
                } else if (mymobile_number.length() < 10) {
                    mobile_number.setError("The Phone number must be at least of 10 characters.");
                    cancel = true;
                } else if (mymobile_number.length() > 10) {
                    mobile_number.setError("The Phone number must be of 10 characters only");
                    cancel = true;
                }else if (TextUtils.isEmpty(mypassword)) {
                    pass_word.setError("Password is required");
                    cancel = true;
                }  else if (pass_word.length() < 8) {
                    pass_word.setError("Password minimum contain 6 character");
                    cancel = true;
                }else {

                    signUpMobile();


                }
            }
        });
    }

    private void signUpMobile() {

        networkCall.NetworkAPICall(ApiURL.user_register_with_mobile, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void loginphone(View view) {
        Intent intent2 = new Intent(phoneRegistration.this, phoneLogin.class);
        startActivity(intent2);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_register_with_mobile:
                ion = (Builders.Any.B) Ion.with(phoneRegistration.this)
                        .load("POST", ApiURL.user_register_with_mobile)

                        .setBodyParameter("mobile", mymobile_number)
                        .setBodyParameter("password", mypassword);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {

        switch (apitype) {
            case ApiURL.user_register_with_mobile:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        Toast.makeText(phoneRegistration.this, "successfully log-in", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(phoneRegistration.this, phoneLogin.class);
                        startActivity(intent);
                        finish();
                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(phoneRegistration.this, "The phone no. is already registered.".toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(phoneRegistration.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }


    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}