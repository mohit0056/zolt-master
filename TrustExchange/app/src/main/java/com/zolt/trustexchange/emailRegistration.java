package com.zolt.trustexchange;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONException;
import org.json.JSONObject;


public class emailRegistration extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    TextView textView;
    EditText first_name, last_name, pass_word, email_id, confirm_password;
    Button btnEmailRegister;
    CheckBox chkCondition;
    Boolean cancel;
    Progress progress;
    NetworkCall networkCall;
    String mfirstnm, mlastnm, memailid, mpassword, mconfpass;
    ImageView hidebtn, confhidebtn;
    private boolean isPasswordView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_registration);
        progress = new Progress(emailRegistration.this);
        networkCall = new NetworkCall(emailRegistration.this, emailRegistration.this);


        textView = (TextView) findViewById(R.id.registerwithmobile);
        first_name = (EditText) findViewById(R.id.firstnameEditText);
        last_name = (EditText) findViewById(R.id.lastnameEditText);
        email_id = (EditText) findViewById(R.id.emailEditText);
        pass_word = (EditText) findViewById(R.id.passwordEditText);
        chkCondition = (CheckBox) findViewById(R.id.checkBox);
        confirm_password = (EditText) findViewById(R.id.confpssawordEditText);

        hidebtn = (ImageView)findViewById(R.id.hideBtn);
        hidebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordView) {
                    hidebtn.setBackgroundResource(R.drawable.view);
                    pass_word.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isPasswordView = false;
                } else {
                    hidebtn.setBackgroundResource(R.drawable.hide);
                    pass_word.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isPasswordView = true;
                }

            }
        });
        confhidebtn = (ImageView)findViewById(R.id.confhideBtn);
        confhidebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordView) {
                    confhidebtn.setBackgroundResource(R.drawable.view);
                    confirm_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isPasswordView = false;
                } else {
                    confhidebtn.setBackgroundResource(R.drawable.hide);
                    confirm_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isPasswordView = true;
                }            }
        });
        btnEmailRegister = (Button) findViewById(R.id.btnEmailRegister);
        btnEmailRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mfirstnm = first_name.getText().toString().trim();
                mlastnm = last_name.getText().toString().trim();
                memailid = email_id.getText().toString().trim();
                mpassword = pass_word.getText().toString().trim();
                mconfpass = confirm_password.getText().toString().trim();

                first_name.setError(null);
                last_name.setError(null);
                email_id.setError(null);

                pass_word.setError(null);
                confirm_password.setError(null);


                if (TextUtils.isEmpty(mfirstnm)) {
                    first_name.setError("First Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mlastnm)) {
                    last_name.setError("Last Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(memailid)) {
                    email_id.setError("Email id is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mpassword)) {
                    pass_word.setError("Password is required");
                    cancel = true;
                }  else if (pass_word.length() < 8) {
                    pass_word.setError("Password minimum contain 6 character");
                    cancel = true;
                }else if (TextUtils.isEmpty(mconfpass)) {
                    confirm_password.setError("Reenter Password is required");
                    cancel = true;
                } else if (!mconfpass.equals(mpassword)) {
                    confirm_password.setError("Both Passwords are Different");
                    cancel = true;
                } else {

                    signUp();


                }
            }
        });


        textView = (TextView) findViewById(R.id.registerwithmobile);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(emailRegistration.this, phoneRegistration.class);
                startActivity(intent1);

            }
        });

//        btnEmailRegister = (Button) findViewById(R.id.btnEmailRegister);
//        btnEmailRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                btnEmailRegister.setBackgroundColor(getResources().getColor(R.color.redcheck));
//                Intent intent2 = new Intent(emailRegistration.this, MainActivity.class);
//                startActivity(intent2);
//
//            }
//        });
    }

    private void signUp() {
        networkCall.NetworkAPICall(ApiURL.user_register_with_email, true);
    }

    public void back(View view) {
        super.onBackPressed();
    }


    public void loginemail(View view) {
        Intent intent3 = new Intent(emailRegistration.this, emailLogin.class);
        startActivity(intent3);

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_register_with_email:
                ion = (Builders.Any.B) Ion.with(emailRegistration.this)
                        .load("POST", ApiURL.user_register_with_email)
                        .setBodyParameter("firstname", mfirstnm)
                        .setBodyParameter("lastname", mlastnm)
                        .setBodyParameter("email", memailid)
                        .setBodyParameter("password", mpassword)
                        .setBodyParameter("confirm_password", mconfpass);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.user_register_with_email:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        Toast.makeText(emailRegistration.this, "successfully log-in", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(emailRegistration.this, emailLogin.class);
                        startActivity(intent);
                        finish();
                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(emailRegistration.this, "The email has already been taken.".toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(emailRegistration.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(emailRegistration.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}