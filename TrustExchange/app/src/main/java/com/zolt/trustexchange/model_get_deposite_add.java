package com.zolt.trustexchange;

public class model_get_deposite_add {

    String memo,address;

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public model_get_deposite_add(String memo_num, String address_id) {

        this.memo = memo_num;
        this.address = address_id;
    }


}
