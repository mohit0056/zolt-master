package com.zolt.trustexchange;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONException;
import org.json.JSONObject;

public class                    inr_withdraw extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    EditText entAmountwithdraw;
    Button withdrawBtn;
    SharedPreferences mSharedPreference;
    int num=0;
    Boolean cancel;
    Progress progress;
    NetworkCall networkCall;
    String myEntAmountwithdraw,userid,myINR;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inr_withdraw);

        progress = new Progress(inr_withdraw.this);
        networkCall = new NetworkCall(inr_withdraw.this, inr_withdraw.this);
        mSharedPreference= PreferenceManager.getDefaultSharedPreferences(inr_withdraw.this);

        userid=mSharedPreference.getString("id", "");
        myINR=mSharedPreference.getString("wallet_amount", "");

        entAmountwithdraw = (EditText) findViewById(R.id.EnterAmountForWithdrawal);

        withdrawBtn = (Button) findViewById(R.id.withdrawalButton);
        withdrawBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                myEntAmountwithdraw = entAmountwithdraw.getText().toString().trim();

                entAmountwithdraw.setError(null);

                Integer addmoney = Integer.parseInt(myEntAmountwithdraw);
                Integer newmyINR = Integer.parseInt(myINR);

                if (TextUtils.isEmpty(myEntAmountwithdraw)) {
                    entAmountwithdraw.setError("This field is required");
                    cancel = true;
                } else if ((addmoney <= 0)){
                    entAmountwithdraw.setError("Please enter amount more than 0");
                    cancel = true;
                }else if((addmoney > newmyINR)){
                    entAmountwithdraw.setError("You do not have sufficient amount to withdraw");
                    cancel = true;
                }else {

                    withdrawMoney();


                }
            }
        });

    }

    private void withdrawMoney() {
        networkCall.NetworkAPICall(ApiURL.withDrawMoney, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.withDrawMoney:
                ion = (Builders.Any.B) Ion.with(inr_withdraw.this)
                        .load("POST", ApiURL.withDrawMoney)
                        .setBodyParameter("amount", myEntAmountwithdraw)
                        .setBodyParameter("user_id", userid)
                      ;
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.withDrawMoney:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        Toast.makeText(inr_withdraw.this, "You have successfully withdrawal the money..Please wait for confirmation.", Toast.LENGTH_SHORT).show();

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(inr_withdraw.this, "Check every field and try again...".toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(inr_withdraw.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

        Toast.makeText(inr_withdraw.this, jsonstring, Toast.LENGTH_SHORT).show();


    }
}