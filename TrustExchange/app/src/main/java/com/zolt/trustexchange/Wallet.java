package com.zolt.trustexchange;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public class Wallet extends AppCompatActivity {
    ViewPager mViewPager,viewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        tabLayout = findViewById(R.id.walletItemsLayout);

        viewPager = findViewById(R.id.walletItemsPager);
     //   tabLayout.addTab(tabLayout.newTab().setText("Overview"));
        tabLayout.addTab(tabLayout.newTab().setText("Spot"));
       // tabLayout.addTab(tabLayout.newTab().setText("Margin"));
        tabLayout.addTab(tabLayout.newTab().setText("Futures"));
//        tabLayout.addTab(tabLayout.newTab().setText("P2P"));
//        tabLayout.addTab(tabLayout.newTab().setText("Savings"));
//        tabLayout.addTab(tabLayout.newTab().setText("Pool"));

//        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);

        final adapter_wallet adapter = new adapter_wallet(this,getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
}