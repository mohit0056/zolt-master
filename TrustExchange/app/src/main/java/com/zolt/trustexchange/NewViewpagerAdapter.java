package com.zolt.trustexchange;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NewViewpagerAdapter extends PagerAdapter {

    private final Activity activity;
    private List<Bannerpojo> arrayList = new ArrayList<>();
    private pageonClick pageonClick;
    private String pageType;


    public interface pageonClick {
        void viewPageImageClick(String page, int position);
    }
    public NewViewpagerAdapter(Activity activity, List<Bannerpojo> arrayList, pageonClick pageonClick, String pageType) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.pageType = pageType;
        this.pageonClick = pageonClick;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private View view = null;

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.layout_view_pager, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.viewpager_image);
        Bannerpojo banners = arrayList.get(position);
        final String name = banners.getWithdrawal_request();
        Picasso.with(activity.getApplicationContext()).load(name).fit()
                .placeholder(R.drawable.ic_launcher_background).
                into(imageView);
        ((ViewPager) container).addView(view);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pageonClick.viewPageImageClick(pageType, position);
            }
        });
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
