package com.zolt.trustexchange;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;

public class main_get_deposite_address extends AppCompatActivity implements adapter_get_deposite_add.ReturnView, NetworkCall.MyNetworkCallBack {
    RecyclerView recypets;
    adapter_get_deposite_add adapter_get_deposite_add;
    Progress progress;
    NetworkCall networkCall;
    ArrayList<model_get_deposite_add> arrListaddpet = new ArrayList<>();
    URI uri;

    ImageView bckrrw;
    String memo_id = "",address_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_get_deposite_address);

        progress = new Progress(main_get_deposite_address.this);
        networkCall=new NetworkCall(main_get_deposite_address.this,main_get_deposite_address.this);
        recypets=(RecyclerView)findViewById(R.id.recycler);

        getAddress();
    }
    private void getAddress() {
        networkCall.NetworkAPICall(ApiURL.get_deposite_address,true);
    }
    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.get_deposite_address:
                ion = (Builders.Any.B) Ion.with(main_get_deposite_address.this)
                        .load("GET", ApiURL.get_deposite_address);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.get_deposite_address:
                try {
                    arrListaddpet.clear();
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                     memo_id = jsonObject.getString("memo");
                     address_id = jsonObject.getString("address");

//                    name.setText(mname);
//                    email.setText(memail);

                } catch (JSONException e1) {


                }
                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }


}