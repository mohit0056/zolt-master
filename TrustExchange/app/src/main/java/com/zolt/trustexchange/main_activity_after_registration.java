package com.zolt.trustexchange;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class main_activity_after_registration extends AppCompatActivity {
    RelativeLayout r1, r2, r3, r4, r1c, r2c, r3c, r4c;
    ImageView i1, i2, i3, i4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_after_registration);

        r1 = (RelativeLayout) findViewById(R.id.btc_content);
        r1c = (RelativeLayout) findViewById(R.id.btc_graph_content);

        r2 = (RelativeLayout) findViewById(R.id.usdt_content);
        r2c = (RelativeLayout) findViewById(R.id.usdt_graph_content);

        r3 = (RelativeLayout) findViewById(R.id.busd_content);
        r3c = (RelativeLayout) findViewById(R.id.busd_graph_content);

        r4 = (RelativeLayout) findViewById(R.id.bnb_content);
        r4c = (RelativeLayout) findViewById(R.id.bnb_graph_content);

        i1 = (ImageView) findViewById(R.id.img_small_btc);
        i2 = (ImageView) findViewById(R.id.img_small_usdt);
        i3 = (ImageView) findViewById(R.id.img_small_busd);
        i4 = (ImageView) findViewById(R.id.img_small_bnb);

        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r1c.setVisibility(View.VISIBLE);
                i1.setVisibility(View.INVISIBLE);
                i4.setVisibility(View.VISIBLE);
                i2.setVisibility(View.VISIBLE);
                i3.setVisibility(View.VISIBLE);

                r2c.setVisibility(View.GONE);
                r3c.setVisibility(View.GONE);
                r4c.setVisibility(View.GONE);

            }
        });

        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r2c.setVisibility(View.VISIBLE);
                r1c.setVisibility(View.GONE);
                i2.setVisibility(View.INVISIBLE);
                i1.setVisibility(View.VISIBLE);
                i4.setVisibility(View.VISIBLE);
                i3.setVisibility(View.VISIBLE);

                r3c.setVisibility(View.GONE);
                r4c.setVisibility(View.GONE);


            }
        });

        r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r1c.setVisibility(View.GONE);
                i3.setVisibility(View.INVISIBLE);
                i1.setVisibility(View.VISIBLE);
                i2.setVisibility(View.VISIBLE);
                i4.setVisibility(View.VISIBLE);

                r2c.setVisibility(View.GONE);
                r3c.setVisibility(View.VISIBLE);
                r4c.setVisibility(View.GONE);
            }
        });

        r4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r1c.setVisibility(View.GONE);
                r2c.setVisibility(View.GONE);
                i4.setVisibility(View.INVISIBLE);
                i1.setVisibility(View.VISIBLE);
                i2.setVisibility(View.VISIBLE);
                i3.setVisibility(View.VISIBLE);


                r3c.setVisibility(View.GONE);
                r4c.setVisibility(View.VISIBLE);
            }
        });

    }

    public void goback(View view) {
        super.onBackPressed();
    }
}