package com.zolt.trustexchange;

import android.widget.Button;

import java.io.Serializable;

public class Model_buy_funjo implements Serializable {

    private String username;

        private int sell_id;

        private int user_id;

        private String funjon_rate;

        private String funjon_sell_coin;

        public void setUsername(String username){
            this.username = username;
        }
        public String getUsername(){
            return this.username;
        }
        public void setSell_id(int sell_id){
            this.sell_id = sell_id;
        }
        public int getSell_id(){
            return this.sell_id;
        }
        public void setUser_id(int user_id){
            this.user_id = user_id;
        }
        public int getUser_id(){
            return this.user_id;
        }
        public void setFunjon_rate(String funjon_rate){
            this.funjon_rate = funjon_rate;
        }
        public String getFunjon_rate(){
            return this.funjon_rate;
        }
        public void setFunjon_sell_coin(String funjon_sell_coin){
            this.funjon_sell_coin = funjon_sell_coin;
        }
        public String getFunjon_sell_coin(){
            return this.funjon_sell_coin;
        }
    }


