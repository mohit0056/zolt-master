package com.zolt.trustexchange;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link trades_spot#newInstance} factory method to
 * create an instance of this fragment.
 */
public class trades_spot extends Fragment implements NetworkCall.MyNetworkCallBack{
    private Button buygreen, sellred, buybtn;
    SharedPreferences mSharedPreference;
    public static final String MyPREFERENCES = "MyPrefs";
    EditText ent_funjo,ent_funjo_value;
    private String myent_funjo,myent_funjo_value,userid , funjo_coins,funjo_coins_value;
    Progress progress;
    Boolean cancel;

    SharedPreferences.Editor editor;
    NetworkCall networkCall;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public trades_spot() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment trades_spot.
     */
    // TODO: Rename and change types and number of parameters
    public static trades_spot newInstance(String param1, String param2) {
        trades_spot fragment = new trades_spot();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_trades_spot, container, false);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        editor = prefs.edit();

        progress = new Progress(getContext());
        networkCall = new NetworkCall(trades_spot.this, getContext());

        userid=(mSharedPreference.getString("id", ""));
        funjo_coins=(mSharedPreference.getString("funjon_rate", ""));
        funjo_coins_value=(mSharedPreference.getString("funjon_sell_coin", ""));



        buygreen = (Button) v.findViewById(R.id.buy_green);
        buybtn = (Button) v.findViewById(R.id.buy_btc_trades);
        sellred = (Button) v.findViewById(R.id.sell_funjo);

        ent_funjo = (EditText)v.findViewById(R.id.enter_funjo_coin);
        ent_funjo_value = (EditText)v.findViewById(R.id.enter_funjo_value);


//        buygreen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                buygreen.setBackground(getResources().getDrawable(R.drawable.buygreen));
//
//                sellred.setBackground(getResources().getDrawable(R.drawable.s3));
//                buybtn.setBackgroundColor(getResources().getColor(R.color.green_buy));
//                buybtn.setText("Buy FIO");
//            }
//        });

        sellred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                 myent_funjo = ent_funjo.getText().toString().trim();
                myent_funjo_value = ent_funjo_value.getText().toString().trim();


                ent_funjo.setError(null);
                ent_funjo_value.setError(null);



                if (TextUtils.isEmpty(myent_funjo)) {
                    ent_funjo.setError("First Name is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myent_funjo_value)) {
                    ent_funjo_value.setError("Last Name is required");
                    cancel = true;
                }
                else {

                    UploadSell();


                }

            }
        });



        return v;

    }

    private void UploadSell() {

        networkCall.NetworkAPICall(ApiURL.sell_funjo, true);

    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.sell_funjo:
                ion = (Builders.Any.B) Ion.with(getContext())
                        .load("POST", ApiURL.sell_funjo)
                        .setBodyParameter("funjon_rate", myent_funjo)
                        .setBodyParameter("funjon_sell_coin", myent_funjo_value)
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.sell_funjo:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                        for (int i = 0; i < jsonObject1.length(); i++) {

//                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);

                            funjo_coins = jsonObject1.getString("funjon_rate");
                            funjo_coins_value = jsonObject1.getString("funjon_sell_coin");


                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                            SharedPreferences.Editor editor = prefs.edit();



                            editor.putString("funjon_rate", funjo_coins);
                            editor.putString("funjon_sell_coin", funjo_coins_value);
                            editor.commit();

//                        }
                        Toast.makeText(getContext(), "You have uploaded your values successfully ", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(getContext(), "Check all the fields carefully".toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(getContext(), jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(getContext(), jsonstring, Toast.LENGTH_SHORT).show();

    }
}