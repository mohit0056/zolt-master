package com.zolt.trustexchange;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class deposite_inr_standard_deposit extends AppCompatActivity {
Button add_transfer_details_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposite_inr_standard_deposit);

        add_transfer_details_btn = findViewById(R.id.add_transfer_details_btn);
        add_transfer_details_btn.setOnClickListener(v -> {
            Intent intent = new Intent(deposite_inr_standard_deposit.this,deposite_inr_standard_deposit_add_details.class);
            startActivity(intent);
        });
    }

    public void goback(View view) {
        super.onBackPressed();
    }
}