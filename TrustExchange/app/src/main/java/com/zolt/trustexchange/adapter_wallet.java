package com.zolt.trustexchange;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class adapter_wallet extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public adapter_wallet(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
//            case 0:
//                wallet_overview wallet_overwiewFragment = new wallet_overview();
//                return wallet_overwiewFragment;
            case 0:
                wallet_spot wallet_spotFragment = new wallet_spot();
                return wallet_spotFragment;
//            case 2:
//                wallet_margin wallet_marginFragment = new wallet_margin();
//                return wallet_marginFragment;
            case 1:
                wallet_futures wallet_futuresFragment = new wallet_futures();
                return wallet_futuresFragment;
//            case 4:
//                wallet_p2p wallet_p2pFragment = new wallet_p2p();
//                return wallet_p2pFragment;
//            case 5:
//                wallet_savings wallet_savingsFragment = new wallet_savings();
//                return wallet_savingsFragment;
//
//            case 6:
//                wallet_pool wallet_poolFragment = new wallet_pool();
//                return wallet_poolFragment;


            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}
