package com.zolt.trustexchange;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class emailLogin extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    TextView textView;
    EditText pass_word, email_id;
    Button btnEmailLogin;
    SharedPreferences mSharedPreference;
    Boolean cancel;
    private boolean isPasswordView;

    static final String pref_name="Trust_exchange";
    Progress progress;
    String userid , myemail;
    NetworkCall networkCall;
    String lmailid, lpassword, l_email, id;
    Boolean isLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference= PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid=(mSharedPreference.getString("id", ""));
        isLogin= mSharedPreference.getBoolean("login",false);
        if (isLogin) {
            Intent intent = new Intent(emailLogin.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }


        progress = new Progress(emailLogin.this);
        networkCall = new NetworkCall(emailLogin.this, emailLogin.this);
        email_id = findViewById(R.id.emailLoginEditText);
        pass_word = findViewById(R.id.passwordLoginEditText);
        ImageView  hidebtn = findViewById(R.id.hideBtn);
        hidebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordView) {
                    hidebtn.setBackgroundResource(R.drawable.view);
                    pass_word.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isPasswordView = false;
                } else {
                    hidebtn.setBackgroundResource(R.drawable.hide);
                    pass_word.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isPasswordView = true;
                }
            }
        });

        btnEmailLogin = (Button) findViewById(R.id.btnEmailLogin);
        btnEmailLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                lmailid = email_id.getText().toString().trim();
                lpassword = pass_word.getText().toString().trim();
                email_id.setError(null);
                pass_word.setError(null);

                if (TextUtils.isEmpty(lmailid)) {
                    email_id.setError("Email id is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(lpassword)) {
                    pass_word.setError("Password is required");
                    cancel = true;
                } else {
                    custmorelogin();


                }
            }
        });


    }

    private void custmorelogin() {
        networkCall.NetworkAPICall(ApiURL.user_login_with_email, true);


    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void loginmobile(View view) {
        Intent intent1 = new Intent(emailLogin.this, phoneLogin.class);
        startActivity(intent1);
    }

    public void forgotpassEmail(View view) {
        Intent intent2 = new Intent(emailLogin.this, otpVerification.class);
        startActivity(intent2);
    }

    public void emailregister(View view) {
        Intent intent3 = new Intent(emailLogin.this, emailRegistration.class);
        startActivity(intent3);
    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_login_with_email:
                ion = (Builders.Any.B) Ion.with(emailLogin.this)
                        .load("POST", ApiURL.user_login_with_email)
                        .setBodyParameter("email", lmailid)
                        .setBodyParameter("password", lpassword);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.user_login_with_email:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("satus");

                    if (status.equals("true")) {

                        JSONArray jsonObject1= jsonObject.getJSONArray("data");

                        for (int i = 0; i <jsonObject1.length(); i++) {
                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);
                            userid = Jasonobject1.getString("id");
                             myemail = Jasonobject1.getString("email");
                        }
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("email", myemail);
                            editor.putString("id", userid);
                            editor.commit();
                            mSharedPreference.edit().putBoolean("login", true).commit();

                            Toast.makeText(emailLogin.this, "Sucessfully Logged in", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(emailLogin.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                        String fail_status = jsonObject.getString("satus");
                        String massage = jsonObject.getString("message");
                        if(fail_status.equals("false")){
                            Toast.makeText(this, massage, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(emailLogin.this, ""+e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(emailLogin.this, jsonstring, Toast.LENGTH_SHORT).show();

    }
}