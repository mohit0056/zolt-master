package com.zolt.trustexchange;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class adapter_market_future extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public adapter_market_future(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                future_favorites futureFavorites = new future_favorites();
                return futureFavorites;
            case 1:
                future_usdt futureUsdt = new future_usdt();
                return futureUsdt;
            case 2:
                future_coin futureCoin = new future_coin();
                return futureCoin;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}
