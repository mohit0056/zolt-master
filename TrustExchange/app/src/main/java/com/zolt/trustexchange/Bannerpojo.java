package com.zolt.trustexchange;

import java.io.Serializable;

public class Bannerpojo implements Serializable {

        private int amount;

        private String withdrawal_request;

        public void setAmount(int amount){
            this.amount = amount;
        }
        public int getAmount(){
            return this.amount;
        }
        public void setWithdrawal_request(String withdrawal_request){
            this.withdrawal_request = withdrawal_request;
        }
        public String getWithdrawal_request(){
            return this.withdrawal_request;
        }

    }


