package com.zolt.trustexchange;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link wallet_spot#newInstance} factory method to
 * create an instance of this fragment.
 */
public class wallet_spot extends Fragment {

    String userid,myINR,myFujocoins,MyINRUP;
    SharedPreferences mSharedPreference;
    TextView amountTxt;
RelativeLayout inr_btn;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public wallet_spot() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment wallet_spot.
     */
    // TODO: Rename and change types and number of parameters
    public static wallet_spot newInstance(String param1, String param2) {
        wallet_spot fragment = new wallet_spot();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v = inflater.inflate(R.layout.fragment_wallet_spot, container, false);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());

        myINR=(mSharedPreference.getString("wallet_amount", ""));

amountTxt = (TextView)v.findViewById(R.id.amount);

amountTxt.setText(myINR);

        inr_btn = (RelativeLayout)v.findViewById(R.id.inr_wallet_btn);
   inr_btn.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           Intent inr_wallet = new Intent(getActivity(),Inr_wallet.class);
           startActivity(inr_wallet);

       }
   });




   return v;
    }
}