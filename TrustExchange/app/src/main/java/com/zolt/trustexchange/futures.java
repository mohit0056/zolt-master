package com.zolt.trustexchange;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public class futures extends AppCompatActivity {
    ViewPager mViewPager,viewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_futures
        );

        tabLayout = findViewById(R.id.future_main_tab);
        viewPager = findViewById(R.id.future_main_Pager);
        tabLayout.addTab(tabLayout.newTab().setText("USDT-m"));
        tabLayout.addTab(tabLayout.newTab().setText("COIN-m"));
        tabLayout.addTab(tabLayout.newTab().setText("Options"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final adapter_futures adapter = new adapter_futures(this,getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    public void goback(View view) {
        super.onBackPressed();
    }
}