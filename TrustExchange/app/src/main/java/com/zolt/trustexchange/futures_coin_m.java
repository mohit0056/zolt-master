package com.zolt.trustexchange;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link futures_coin_m#newInstance} factory method to
 * create an instance of this fragment.
 */
public class futures_coin_m extends Fragment implements AdapterView.OnItemSelectedListener{
    private Button buygreen, sellred, buybtn;
    private String[] cross={"Cross"};
    private String[] limit={"Limit","Market","Stop Limit","OCO"};

    private String[] profit={"20x"};
    private String[] num={"0.1","1","10"};
            // TODO: Rename parameter arguments, choose names that match
            // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

            // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public futures_coin_m() {
           // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment futures_coin_m.
     */
    // TODO: Rename and change types and number of parameters
    public static futures_coin_m newInstance(String param1, String param2) {
        futures_coin_m fragment = new futures_coin_m();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_futures_coin_m, container, false);
        buygreen = v.findViewById(R.id.buy_green);
        buybtn = v.findViewById(R.id.buy_btc_trades);
        sellred = v.findViewById(R.id.sell_red);
        Spinner spin1 = v.findViewById(R.id.spinner1);
        spin1.setOnItemSelectedListener(this);

        Spinner spin2 = v.findViewById(R.id.spinner2);
        spin2.setOnItemSelectedListener(this);

        Spinner spin3 = v.findViewById(R.id.spinner3);
        spin3.setOnItemSelectedListener(this);

        Spinner spin4 = v.findViewById(R.id.limit);
        spin4.setOnItemSelectedListener(this);

        buygreen.setOnClickListener(v1 -> {
            buygreen.setBackground(getResources().getDrawable(R.drawable.buygreen));

            sellred.setBackground(getResources().getDrawable(R.drawable.s3));
            buybtn.setBackgroundColor(getResources().getColor(R.color.green_buy));
            buybtn.setText("Log In");
        });

        sellred.setOnClickListener(v12 -> {
            sellred.setBackground(getResources().getDrawable(R.drawable.sellred));
            buygreen.setBackground(getResources().getDrawable(R.drawable.s3));
            buybtn.setBackgroundColor(getResources().getColor(R.color.red_sell));
            buybtn.setText("Log In");
        });

        ArrayAdapter aa = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,cross);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin1.setAdapter(aa);

        ArrayAdapter bb = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,profit);
        bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin2.setAdapter(bb);

        ArrayAdapter cc = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,num);
        cc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin3.setAdapter(cc);

        ArrayAdapter dd = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,limit);
        dd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin4.setAdapter(dd);


        return v;

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        Toast.makeText(getActivity(), cross[position], Toast.LENGTH_LONG).show();
//        Toast.makeText(getActivity(), money[position], Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}