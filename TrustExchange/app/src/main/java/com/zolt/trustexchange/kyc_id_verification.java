package com.zolt.trustexchange;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link kyc_id_verification#newInstance} factory method to
 * create an instance of this fragment.
 */
public class kyc_id_verification extends Fragment {
    ImageView pancard_img;
    private static final int SELECT_PICTURE = 1;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private static final int IMAGE_PICKER_SELECT = 1;
    EditText ent_amount;
    private String selectedImagePath;
    int clickImage;
    Uri selectedMediaUri,selctvideouri,uri;
    Button upload;
    Uri contentURI;
    File File_data;
    Progress progress;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    String userid,filename = "",filePath="",filenameimg="",pan_card_pic="";
    final int RESULT_LOAD_IMG = 1000;
    private static final int PICK_FROM_GALLERY = 1;

    private ImageView img;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public kyc_id_verification() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment kyc_id_verification.
     */
    // TODO: Rename and change types and number of parameters
    public static kyc_id_verification newInstance(String param1, String param2) {
        kyc_id_verification fragment = new kyc_id_verification();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kyc_id_verification, container, false);


        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());

        userid=(mSharedPreference.getString("id", ""));



        pancard_img = (ImageView) v.findViewById(R.id.pancard_upload);
        pancard_img.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try { if (ActivityCompat.checkSelfPermission(getContext(),android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }else {
                    clickImage=1;
                    loadImagefromGallery();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return v;
    }




    private void loadImagefromGallery() {

        if (clickImage == 1) {

            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
            if (data != null) {
                contentURI = data.getData();
                try {
                    Uri selectedImage = data.getData();
                    pancard_img.setImageURI(selectedImage);
                    filePath = getRealPathFromURI(selectedImage);
                    File_data = new File(filePath);


                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("pancardfilePath", filePath);
                    editor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }



        super.onActivityResult(requestCode, resultCode, data);
    }
    private String getRealPathFromURI(Uri uri) {

        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getContext(), uri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }



}