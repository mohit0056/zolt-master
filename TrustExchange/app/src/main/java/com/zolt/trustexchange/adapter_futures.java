package com.zolt.trustexchange;


import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class adapter_futures extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public adapter_futures(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                futures_usdt_m futures_usdt_m = new futures_usdt_m();
                return futures_usdt_m;
            case 1:
                futures_coin_m futures_coin_m = new futures_coin_m();
                return futures_coin_m;
            case 2:
                futures_options_m futures_options_m = new futures_options_m();
                return futures_options_m;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}
