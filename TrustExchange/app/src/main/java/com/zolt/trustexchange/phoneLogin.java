package com.zolt.trustexchange;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class phoneLogin extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    TextView textView;
    EditText pass_word, phone;
    Button btnEmailLogin;
    SharedPreferences mSharedPreference;
    Boolean cancel;
    private boolean isPasswordView;

    static final String pref_name="Trust_exchange";
    Progress progress;
    String userid , myemail;
    NetworkCall networkCall;
    String mypass_word, myphone, id;
    Boolean isLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference= PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid=(mSharedPreference.getString("id", ""));
        isLogin= mSharedPreference.getBoolean("login",false);
        if (isLogin) {
            Intent intent = new Intent(phoneLogin.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        progress = new Progress(phoneLogin.this);
        networkCall = new NetworkCall(phoneLogin.this, phoneLogin.this);

        phone = (EditText) findViewById(R.id.phonemobEditText);
        pass_word = (EditText) findViewById(R.id.passwordEditText);
        ImageView hidebtn = (ImageView)findViewById(R.id.hideBtn);
        hidebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordView) {
                    hidebtn.setBackgroundResource(R.drawable.view);
                    pass_word.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isPasswordView = false;
                } else {
                    hidebtn.setBackgroundResource(R.drawable.hide);
                    pass_word.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isPasswordView = true;
                }
            }
        });



        btnEmailLogin = (Button) findViewById(R.id.btnEmailLogin);
        btnEmailLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                myphone = phone.getText().toString().trim();

                mypass_word = pass_word.getText().toString().trim();

                phone.setError(null);
                pass_word.setError(null);



                if (TextUtils.isEmpty(myphone)) {
                    phone.setError("Phone number is Required");
                    cancel = true;
                } else if (myphone.length() < 10) {
                    phone.setError("The Phone number must be at least of 10 characters.");
                    cancel = true;
                } else if (myphone.length() > 10) {
                    phone.setError("The Phone number must be of 10 characters only");
                    cancel = true;
                }else if (TextUtils.isEmpty(mypass_word)) {
                    pass_word.setError("Password is required");
                    cancel = true;
                }  else if (pass_word.length() < 8) {
                    pass_word.setError("Password minimum contain 6 character");
                    cancel = true;
                }else {

                    signUpMobile();


                }
            }
        });

    }

    private void signUpMobile() {

        networkCall.NetworkAPICall(ApiURL.user_login_with_mobile, true);

    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void emailLogin(View view) {
        Intent intent1 = new Intent(phoneLogin.this, emailLogin.class);
        startActivity(intent1);    }

    public void forgotpassphone(View view) {

        Intent intent2 = new Intent(phoneLogin.this,otpVerification.class);
        startActivity(intent2);
    }

    public void registerwithphone(View view) {
        Intent intent3 = new Intent(phoneLogin.this,phoneRegistration.class);
        startActivity(intent3);
    }



    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.user_login_with_mobile:
                ion = (Builders.Any.B) Ion.with(phoneLogin.this)
                        .load("POST", ApiURL.user_login_with_mobile)
                        .setBodyParameter("mobile", myphone)
                        .setBodyParameter("password", mypass_word);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.user_login_with_mobile:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("status");

                    if (status.equals("true")) {

                        JSONArray jsonObject1= jsonObject.getJSONArray("data");

                        for (int i = 0; i <jsonObject1.length(); i++) {
                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);
                            userid = Jasonobject1.getString("id");
                            myemail = Jasonobject1.getString("mobile");
                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("mobile", myphone);
                        editor.putString("id", userid);
                        editor.commit();
                        mSharedPreference.edit().putBoolean("login", true).commit();

                        Toast.makeText(phoneLogin.this, "Sucessfully Logged in", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(phoneLogin.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        String fail_status = jsonObject.getString("satus");
                        String massage = jsonObject.getString("message");
                        if(fail_status.equals("false")){
                            Toast.makeText(this, massage, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(phoneLogin.this, ""+e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}