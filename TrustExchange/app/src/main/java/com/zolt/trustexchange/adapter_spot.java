package com.zolt.trustexchange;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class adapter_spot extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public adapter_spot(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                favorites favoritesFragment = new favorites();
                return favoritesFragment;
            case 1:
                Market_spot_btc btcFragment = new Market_spot_btc();
                return btcFragment;
            case 2:
                Market_spot_usdt bnbFragment = new Market_spot_usdt();
                return bnbFragment;
            case 3:
                Market_spot_inr altsFragment = new Market_spot_inr();
                return altsFragment;
            case 4:
                Market_spot_trx fiatFragment = new Market_spot_trx();
                return fiatFragment;


            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}