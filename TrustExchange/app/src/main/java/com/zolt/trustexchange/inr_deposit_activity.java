package com.zolt.trustexchange;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class inr_deposit_activity extends AppCompatActivity {
LinearLayout standard_deposit;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inr_deposit_activity);

        standard_deposit = findViewById(R.id.standard_deposit);
        standard_deposit.setOnClickListener(v -> {
            Intent stand_depo = new Intent(inr_deposit_activity.this,deposite_inr_standard_deposit.class);
            startActivity(stand_depo);
        });
    }

    public void goback(View view) {
        super.onBackPressed();
    }
}