package com.zolt.trustexchange;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class adapter_zones extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public adapter_zones(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                zones_allzones zones_allzones = new zones_allzones();
                return zones_allzones;
            case 1:
                zones_defi zones_defi = new zones_defi();
                return zones_defi;
            case 2:
                zones_innovation zones_innovation = new zones_innovation();
                return zones_innovation;

            case 3:
                zones_coins zones_coins = new zones_coins();
                return zones_coins;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}

