package com.zolt.trustexchange;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Inr_wallet extends AppCompatActivity implements NetworkCall.MyNetworkCallBack,WithdrawalHistory_recy_adapter.ReturnView{
    LinearLayout withdraw, deposit;
    TextView inr, funjo , myinr_up;
    Progress progress;
    NetworkCall networkCall;
    String userid,myINR,myFujocoins,MyINRUP , withDrawAmount,date;
    SharedPreferences mSharedPreference;
    RecyclerView recyclerView;
    ArrayList<Model_withdraw_history> arrmodelwithdrawhistory =new ArrayList<>();
    WithdrawalHistory_recy_adapter withdrawalHistory_recy_adapter;
    Model_withdraw_history model_withdraw_history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inr_wallet);
        progress = new Progress(Inr_wallet.this);
        networkCall = new NetworkCall(Inr_wallet.this, Inr_wallet.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid=(mSharedPreference.getString("id", ""));
        myINR=(mSharedPreference.getString("wallet_amount", ""));
        myFujocoins=(mSharedPreference.getString("funjon_amount", ""));
        recyclerView = findViewById(R.id.dipositrecycler);
        inr = findViewById(R.id.inr_value);
        myinr_up = findViewById(R.id.my_inr_up);
        funjo = findViewById(R.id.funzo_value);
        withdraw = findViewById(R.id.withdraw);
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Inr_wallet.this, inr_withdraw.class);
                startActivity(intent);

            }
        });
        deposit = findViewById(R.id.deposit);
        deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(Inr_wallet.this, inr_deposit_activity.class);
                startActivity(intent2);
            }
        });
        Inr_wallet_get();
        getWithrawData();
    }

    private void getWithrawData() {
        networkCall.NetworkAPICall(ApiURL.withdraw_history, true);
    }

    private void Inr_wallet_get() {
        networkCall.NetworkAPICall(ApiURL.My_wallet_details, true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void goback(View view) {
        super.onBackPressed();
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.My_wallet_details:
                ion = (Builders.Any.B) Ion.with(Inr_wallet.this)
                        .load("POST", ApiURL.My_wallet_details)
                        .setBodyParameter("user_id", userid);
                break;

            case ApiURL.withdraw_history:
                ion = (Builders.Any.B) Ion.with(Inr_wallet.this)
                        .load("POST", ApiURL.withdraw_history)
                        .setBodyParameter("user_id",userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.My_wallet_details:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    if (status.equals("true")) {
                        JSONObject jsonObject1= jsonObject.getJSONObject("data");
                        myINR = jsonObject1.getString("wallet_amount");
                        MyINRUP= jsonObject1.getString("wallet_amount");
                        myFujocoins = jsonObject1.getString("funjon_amount");
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("wallet_amount",myINR);
                        editor.putString("funjon_amount", myFujocoins);
                        editor.commit();
                        inr.setText(myINR);
                        myinr_up.setText(MyINRUP);
                        funjo.setText(myFujocoins);

                    } else {


                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(Inr_wallet.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.withdraw_history:
                try {
                    JSONObject withdrawHistory = new JSONObject(jsonstring.toString());
                    String status = withdrawHistory.getString("success");
                    if (status.equals("true")) {

                        JSONArray withdrawHistoryList= withdrawHistory.getJSONArray("data");

                        for (int i = 0; i < withdrawHistoryList.length(); i++) {
                            model_withdraw_history = new Gson().fromJson(withdrawHistoryList.optJSONObject(i).toString(), Model_withdraw_history.class);
                            arrmodelwithdrawhistory.add(model_withdraw_history);


                        }

                        withdrawalHistory_recy_adapter = new WithdrawalHistory_recy_adapter(arrmodelwithdrawhistory,Inr_wallet.this, R.layout.withdraw_history, this, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(Inr_wallet.this));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(withdrawalHistory_recy_adapter);
                    } else {
                        String status_fail = withdrawHistory.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(Inr_wallet.this, "Data not found", Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {
                    Toast.makeText(Inr_wallet.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(Inr_wallet.this, "Error", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {

        Model_withdraw_history model_withdraw_history=arrmodelwithdrawhistory.get(position);

        TextView date= view.findViewById(R.id.date);
        TextView withdrawAmount= view.findViewById(R.id.Withdrawamount);
        model_withdraw_history.getDate();
        model_withdraw_history.getAmount();
        String mdate="";
        mdate=model_withdraw_history.getDate();
        date.setText(mdate);
        String mwithdrawAmount="";
        mwithdrawAmount=model_withdraw_history.getAmount();
        withdrawAmount.setText(mwithdrawAmount);





    }
}