package com.zolt.trustexchange;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.zolt.trustexchange.utill.NetworkCall;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link kyc_address_verification#newInstance} factory method to
 * create an instance of this fragment.
 */
public class kyc_address_verification extends Fragment {
    ImageView imageViewfront, imageViewback;
    private static final int SELECT_PICTURE = 1;
    SharedPreferences mSharedPreference;
    int clickImage;
    private static final int PICK_FROM_GALLERY = 1;
    String filePath="";
    String filePath2="";
    Uri contentURI;
    File File_data;
    File File_data2;
    ImageView pancard_img;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private static final int IMAGE_PICKER_SELECT1 = 1;
    private static final int IMAGE_PICKER_SELECT2 = 1;
    EditText ent_amount;
    private String selectedImagePath,userid;

    Uri selectedMediaUri,selctvideouri,uri;
    Button upload;

    SharedPreferences.Editor editor;
    NetworkCall networkCall;

    String pancardfilePath="";

    final int RESULT_LOAD_IMG = 1000;

    private ImageView img;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public kyc_address_verification() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment kyc_address_verification.
     */
    // TODO: Rename and change types and number of parameters
    public static kyc_address_verification newInstance(String param1, String param2) {
        kyc_address_verification fragment = new kyc_address_verification();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kyc_address_verification, container, false);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getContext());

        userid=(mSharedPreference.getString("id", ""));


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

       editor = prefs.edit();
        pancardfilePath=(mSharedPreference.getString("pancardfilePath", ""));


        imageViewfront = (ImageView) v.findViewById(R.id.adharfrontImage);


        imageViewback = (ImageView) v.findViewById(R.id.adharbackImage);

        imageViewfront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);


                try {
                    if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 1;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        imageViewback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);


                try {
                    if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        clickImage = 2;
                        loadImagefromGallery();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        return v;
    }

    private void loadImagefromGallery() {


        if (clickImage == 1) {

            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else if (clickImage == 2) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (clickImage) {
            case 1:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            imageViewfront.setImageURI(selectedImage);
                            filePath = getRealPathFromURI(selectedImage);
                            File_data = new File(filePath);


                            editor.putString("aadharfrontfilePath", filePath);
                            editor.commit();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case 2:
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                    if (data != null) {
                        contentURI = data.getData();
                        try {
                            Uri selectedImage = data.getData();
                            imageViewback.setImageURI(selectedImage);
                            filePath2 = getRealPathFromURI(selectedImage);
                            File_data2 = new File(filePath2);
                            editor.putString("aadharbackfilePath", filePath2);
                            editor.commit();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
    }

    private String getRealPathFromURI(Uri uri) {

        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getContext(), uri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

}