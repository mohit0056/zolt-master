package com.zolt.trustexchange.utill;

public class ApiURL {
    public final static String main_url = "http://inmortaltech.com/Balvahan-APIs/public/api/";
    public final static String user_register_with_email = main_url+"register_bit";
    public final static String user_login_with_email = main_url+"login_bit";
    public final static String user_profile = main_url+"profile";
    public final static String user_logout = main_url+"logout";
    public final static String get_deposite_address = main_url+"depositAddress";
    public final static String My_wallet_details = main_url+"wallet_bit_get";
    public final static String Deposite_money = main_url+"add_wallet";
    public final static String kyc_verification = main_url+"add_kyc_bit";
    public final static String sell_funjo = main_url+"addfunjon";
    public final static String funjo_buy_list = main_url+"getfunjon";
    public final static String withDrawMoney = main_url+"add_withdrawal";
    public final static String buy_funjo = main_url+"buy_funjon";
    public final static String withdraw_history = main_url+"withdrawal_listing";
    public final static String user_register_with_mobile = main_url+"register_mobile_bit";
    public final static String user_login_with_mobile = main_url+"login_mobile_bit";
    public final static String urlbanner = main_url+"banner_bit";



}
