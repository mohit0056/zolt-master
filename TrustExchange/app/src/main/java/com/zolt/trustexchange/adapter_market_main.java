package com.zolt.trustexchange;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class adapter_market_main extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public adapter_market_main(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                spot spotFragment = new spot();
                return spotFragment;
            case 1:
                future futureFragment = new future();
                return futureFragment;
            case 2:
                zone zoneFragment = new zone();
                return zoneFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}