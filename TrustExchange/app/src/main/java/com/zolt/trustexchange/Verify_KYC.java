package com.zolt.trustexchange;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Verify_KYC extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify__k_y_c);
        loadFragment(new kyc_address_verification());
        //getting bottom navigation view and attaching the listener
        BottomNavigationView navigation = findViewById(R.id.bottomBar);
        navigation.setOnNavigationItemSelectedListener(Verify_KYC.this);
    }

    public void goback(View view) {
        super.onBackPressed();
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {


            case R.id.add_veri:
                fragment = new kyc_address_verification();
                break;
            case R.id.id_veri:
                fragment = new kyc_id_verification();
                break;
            case R.id.decla:
                fragment = new kyc_declaration();
                break;
        }

        return loadFragment(fragment);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}