package com.zolt.trustexchange;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.zolt.trustexchange.utill.ApiURL;
import com.zolt.trustexchange.utill.NetworkCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link gainers#newInstance} factory method to
 * create an instance of this fragment.
 */
public class gainers extends Fragment implements NetworkCall.MyNetworkCallBack,Buyfunjo_recy_adapter.ReturnView{
    Progress progress;
    NetworkCall networkCall;
    RecyclerView recyclerView;
    private String userid,sell_id,funjo_coins,funjo_coins_value,myINR;
    Model_buy_funjo model_buy_funjo;
    ArrayList<Model_buy_funjo>arrmodelbyfunjo=new ArrayList<>();
    Buyfunjo_recy_adapter buyfunjo_recy_adapter;
    Boolean isBuyCoin;
    SharedPreferences mSharedPreference;



    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public gainers() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment gainers.
     */
    // TODO: Rename and change types and number of parameters
    public static gainers newInstance(String param1, String param2) {
        gainers fragment = new gainers();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_gainers, container, false);

        progress = new Progress(getContext());
        networkCall = new NetworkCall(gainers.this, getContext());

        mSharedPreference= PreferenceManager.getDefaultSharedPreferences(getContext());

        userid=mSharedPreference.getString("id", "");
        sell_id=mSharedPreference.getString("sell_id", "");
        funjo_coins=(mSharedPreference.getString("funjon_rate", ""));
        funjo_coins_value=(mSharedPreference.getString("funjon_sell_coin", ""));
        myINR=mSharedPreference.getString("wallet_amount", "");
        recyclerView = v.findViewById(R.id.recycler_buy_funjo);

   getData();

   return v;
    }

    private void getData() {

        networkCall.NetworkAPICall(ApiURL.funjo_buy_list, true);

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.funjo_buy_list:
                ion = (Builders.Any.B) Ion.with(gainers.this)
                        .load("GET", ApiURL.funjo_buy_list)
                        .setBodyParameter("user_id",userid);

                break;

            case ApiURL.buy_funjo:
                ion = (Builders.Any.B) Ion.with(gainers.this)
                        .load("POST", ApiURL.buy_funjo)
                        .setBodyParameter("user_id",userid)
                        .setBodyParameter("sell_id",sell_id);

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.funjo_buy_list:
                try {
                    arrmodelbyfunjo.clear();
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonObject1.length(); i++) {
                            model_buy_funjo = new Gson().fromJson(jsonObject1.optJSONObject(i).toString(), Model_buy_funjo.class);
                            arrmodelbyfunjo.add(model_buy_funjo);
                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);
                            sell_id = Jasonobject1.getString("sell_id");
                        }
                        buyfunjo_recy_adapter = new Buyfunjo_recy_adapter(arrmodelbyfunjo,getContext(), R.layout.modellayout, this, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(buyfunjo_recy_adapter);

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                        SharedPreferences.Editor editor = prefs.edit();

                        editor.putString("sell_id", sell_id);
                        editor.commit();


                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(getContext(), "Data not found".toString(),Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(getContext(), jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.buy_funjo:

                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");

                    if (succes.equals("true")) {
                        Toast.makeText(getContext(), "Sold", Toast.LENGTH_SHORT).show();

                    } else {

                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(getContext(), "Please try again later".toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(getContext(), jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }

                break;


        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

        Toast.makeText(getContext(), jsonstring, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        Model_buy_funjo model_buy_funjo=arrmodelbyfunjo.get(position);
        TextView name_lst= view.findViewById(R.id.name_lst);
        TextView funjo_total_list= view.findViewById(R.id.funjo_total_lst);
        TextView funjo_coin_value_lst= view.findViewById(R.id.coin_value_lst);
        Button buyBTn = view.findViewById(R.id.buy_funjo);

        String mname_list="";
        mname_list=model_buy_funjo.getUsername();
        name_lst.setText(mname_list);

        String mfunjo_coin_list="";
        mfunjo_coin_list=model_buy_funjo.getFunjon_rate();
        funjo_total_list.setText(mfunjo_coin_list);

        String mfunjo_per_value="";
        mfunjo_per_value=model_buy_funjo.getFunjon_sell_coin();
        funjo_coin_value_lst.setText(mfunjo_per_value);

        buyBTn.setOnClickListener(v -> {

            sell_id = String.valueOf(model_buy_funjo.getSell_id());

            buyFunjo();

        });

    }

    private void buyFunjo() {

        networkCall.NetworkAPICall(ApiURL.buy_funjo, true);

    }
}